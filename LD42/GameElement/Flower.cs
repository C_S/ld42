﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD42.GameElement
{
    class Flower
    {
        public Vector2 position;
        public Texture2D texture;
        public bool toDisplay;

        public Flower(Texture2D texture, Vector2 initPosition)
        {
            this.position = initPosition;
            this.texture = texture;
            toDisplay = true;
        }

    }
}
