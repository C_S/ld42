﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LD42.GameElement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LD42.GameElement
{
    class Weed
    {
        public Vector2 position;
        public Texture2D texture;
        public bool toDisplay;
        public int hitBoxWidth;

        public Weed(Texture2D texture, Vector2 initPosition)
        {
            this.position = initPosition;
            this.texture = texture;
            this.toDisplay = true;
            this.hitBoxWidth = 50;
        }

        
    }
}
