﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LD42.GameElement
{
    static class Day
    {
        static public Timer spawnTimer;
        static public int count;
        static public Timer generalTimer;
        static public int timeBetweenSpawn;
        static public float currentTime;
        static public float oldTime;

        static public void NextDay(List<Weed> weeds)
        {

            bool nothingToDisplay = false;

            foreach (Weed w in weeds)
            {
                nothingToDisplay = nothingToDisplay || w.toDisplay;
            }

            if (!nothingToDisplay)
            {
                count++;
                if (count <= 7)
                {
                    LD42.getInstance().prepareLevel();
                }
            }
        }

    }
}
