﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD42.GameElement
{
    class Player
    {
        public int speed;
        
        public Texture2D texture;
        public Vector2 position;
        public int hitBoxWidth;
        public int hitBoxHeight;
        public int boundsWidth;
        public int boundsHeight;
        public Vector2 hitBoxPosition;

        public Player(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.position = position;
            this.speed = 4;
            this.boundsWidth = 25;
            this.boundsHeight = 40;
            this.hitBoxWidth = 10;
            this.hitBoxHeight = 10;
            this.hitBoxPosition = new Vector2(position.X + boundsWidth - hitBoxWidth, position.Y + 15);
            
        }

        public bool playerGetWeed(Weed w)
        {
            if (w.toDisplay)
            {
                this.hitBoxPosition = new Vector2(position.X + boundsWidth - hitBoxWidth, position.Y + 15);

                Vector2 weedBasePoint = new Vector2(w.position.X, w.position.Y);
                Vector2 weedOppositeBasePoint = new Vector2(w.position.X + w.hitBoxWidth, w.position.Y + w.hitBoxWidth);

                Vector2 playBasePoint = new Vector2(this.hitBoxPosition.X, this.hitBoxPosition.Y);
                Vector2 playOppositeBasePoint = new Vector2(this.hitBoxPosition.X + this.hitBoxWidth, this.hitBoxPosition.Y + this.hitBoxHeight);
                

                if (playBasePoint.X >= weedBasePoint.X && playBasePoint.X <= weedOppositeBasePoint.X
                        && playBasePoint.Y >= weedBasePoint.Y && playBasePoint.Y <= weedOppositeBasePoint.Y)
                    return true;

                if (playBasePoint.X >= weedBasePoint.X && playBasePoint.X <= weedOppositeBasePoint.X
                        && playOppositeBasePoint.Y >= weedBasePoint.Y && playOppositeBasePoint.Y <= weedOppositeBasePoint.Y)
                    return true;

                if (playOppositeBasePoint.X >= weedBasePoint.X && playOppositeBasePoint.X <= weedOppositeBasePoint.X
                       && playBasePoint.Y >= weedBasePoint.Y && playBasePoint.Y <= weedOppositeBasePoint.Y)
                    return true;

                if (playOppositeBasePoint.X >= weedBasePoint.X && playOppositeBasePoint.X <= weedOppositeBasePoint.X
                       && playOppositeBasePoint.Y >= weedBasePoint.Y && playOppositeBasePoint.Y <= weedOppositeBasePoint.Y)
                    return true;

            }
            return false;
        }
        
    }
}
