﻿using LD42.GameElement;
using LD42.StatesGame;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Timers;

namespace LD42
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class LD42 : Game
    {
        static LD42 LD42Instance;

        static String stateName;
        static State stateGame;

        // Garden/General

        private SpriteFont font;
        List<SoundEffect> soundEffects;
        Song song;
        bool won;
        bool lose;

        Random rdm;
        int counterSpawnTime;
                
        //static public int weedRemaining;

        enum GardenOccupation
        {
            Free = 0,
            Flower = 1,
            Weed = 2
        }

        static int[,] garden;

        // Player
        Player player;

        // Flower
        List<Flower> flowers = new List<Flower>();

        // Weed
        List<Weed> weeds = new List<Weed>();

        // Initial
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public LD42()
        {
            LD42Instance = this;

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Set these values to the desired width and height of the window
            graphics.PreferredBackBufferWidth = 500;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();

            soundEffects = new List<SoundEffect>();
        }

        public Point getSizeWindow()
        {
            return new Point(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            Day.count = 1;


            // Garden initialize enum
            rdm = new Random();

            garden = new int[10, 12];

            // At first, tiles are aLL free
            for (int cptX = 0; cptX < 10; cptX++)
            {
                for (int cptY = 0; cptY < 12; cptY++)
                {
                    garden[cptX, cptY] = 0;
                }
            }

            // Half of the garden is flower
            for (int cptFlower = 0; cptFlower < 60; cptFlower++)
            {
                int xFlower = rdm.Next(0, 10);
                int yFlower = rdm.Next(1, 12);

                if (garden[xFlower, yFlower] != 1)
                    garden[xFlower, yFlower] = 1;
                else
                    cptFlower--;
            }

            this.prepareLevel();

            player = new Player(this.Content.Load<Texture2D>("Perso/player"), new Vector2(10, 20));

            stateName = "MENU";
            stateGame = new State(stateName, GraphicsDevice);

            base.Initialize();
        }

        public void prepareLevel()
        {
            this.IsMouseVisible = false;
            weeds.Clear();

            Day.oldTime += Day.currentTime;
            Day.currentTime = 0f;
            
            // One day equals 60 seconds
            switch (Day.count)
            {
                case 1:
                    Day.timeBetweenSpawn = 15000;
                    break;
                case 2:
                    Day.timeBetweenSpawn = 12000;
                    break;
                case 3:
                    Day.timeBetweenSpawn = 10000;
                    break;
                case 4:
                    Day.timeBetweenSpawn = 8000;
                    break;
                case 5:
                    Day.timeBetweenSpawn = 6000;
                    break;
                case 6:
                    Day.timeBetweenSpawn = 4000;
                    break;
                case 7:
                    Day.timeBetweenSpawn = 1000;
                    break;
                default:
                    break;
            }

            counterSpawnTime = 60000 / Day.timeBetweenSpawn;

            //Day.spawnTimer = new Timer();
            //Day.spawnTimer.Start();



            //weedRemaining = counterSpawnTime;

            // Depends on day, number of weed changes
            for (int cptWeed = 0; cptWeed < counterSpawnTime; cptWeed++)
            {
                int xWeed = rdm.Next(0, 10);
                int yWeed = rdm.Next(1, 12);

                if (garden[xWeed, yWeed] != 1)
                    garden[xWeed, yWeed] = 2;
                else
                    cptWeed--;
            }

            // Garden initialize tiles
            for (int cptX = 0; cptX < 10; cptX++)
            {
                for (int cptY = 0; cptY < 12; cptY++)
                {
                    switch (garden[cptX, cptY])
                    {
                        case 1:
                            flowers.Add(new Flower(this.Content.Load<Texture2D>("Floor/flower"), new Vector2(50 * cptX, 50 * cptY)));
                            break;
                        case 2:
                            weeds.Add(new Weed(this.Content.Load<Texture2D>("Floor/weed"), new Vector2(50 * cptX, 50 * cptY)));
                            break;
                    }
                }
            }

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            if (stateName == "MENU")
            { }
            else
            {
                this.song = Content.Load<Song>("Music/song");
                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = true;

                MediaPlayer.Volume = 0.1f;

                soundEffects.Add(Content.Load<SoundEffect>("Music/weedCollided"));
                soundEffects.Add(Content.Load<SoundEffect>("Music/flowerGoodJob"));
                soundEffects.Add(Content.Load<SoundEffect>("Music/flowerGameOver"));
            }

            font = Content.Load<SpriteFont>("font");
            

            // TODO: use this.Content to load your game content here
            
        }

        void MediaPlayer_MediaStateChanged(object sender, System.
                                           EventArgs e)
        {
            // 0.0f is silent, 1.0f is full volume
            MediaPlayer.Volume = 0.01f;
            MediaPlayer.Play(song);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            player.position.X = Math.Min(Math.Max(-10, player.position.X), graphics.PreferredBackBufferWidth - 40);
            player.position.Y = Math.Min(Math.Max(0, player.position.Y), graphics.PreferredBackBufferHeight - 50);
            

            Day.currentTime = (float)gameTime.TotalGameTime.TotalSeconds - Day.oldTime;

            // TODO: Add your update logic here
            if (Day.currentTime < 60 && Day.count <= 7)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                    player.position.Y += player.speed;
                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                    player.position.Y -= player.speed;
                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                    player.position.X -= player.speed;
                if (Keyboard.GetState().IsKeyDown(Keys.Right))
                    player.position.X += player.speed;

                //Day.timeBetweenSpawn += gameTime.ElapsedGameTime.Milliseconds;

                //Collision

                bool hasBeenTouched = false;

                //Treatment of the collision
                foreach (Weed w in weeds)
                {
                    if (player.playerGetWeed(w))
                    {
                        soundEffects[0].Play(volume: 0.4f, pitch: 0.0f, pan: 0.0f);
                        
                        garden[(int)(w.position.X / 50), (int)(w.position.Y / 50)] = 0;

                        w.toDisplay = false;
                        hasBeenTouched = true;
                    }
                }

                // 
                if (hasBeenTouched)
                    Day.NextDay(weeds);
            }
            else
            {
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    Day.count = 1;
                    prepareLevel();
                    won = false;
                    lose = false;
                }
            }
            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            if (stateName == "MENU")
            {
                stateGame.Draw(spriteBatch);
            }
            else
            {
                if (Day.currentTime < 60 && Day.count <= 7)
                {
                    // Garden initialize tiles          

                    for (int cptX = 0; cptX < 10; cptX++)
                    {
                        for (int cptY = 0; cptY < 12; cptY++)
                        {
                            if (garden[cptX, cptY] == 0 || garden[cptX, cptY] == 2)
                                spriteBatch.Draw(this.Content.Load<Texture2D>("Floor/floorGarden"), destinationRectangle: new Rectangle(50 * cptX, 50 * cptY, 50, 50));
                        }
                    }

                    foreach (Flower f in flowers)
                    {
                        if (f.toDisplay == true)
                        {
                            spriteBatch.Draw(f.texture, destinationRectangle: new Rectangle((int)f.position.X, (int)f.position.Y, 50, 50));
                        }
                    }

                    foreach (Weed w in weeds)
                    {
                        if (w.toDisplay == true)
                        {
                            spriteBatch.Draw(w.texture, destinationRectangle: new Rectangle((int)w.position.X, (int)w.position.Y, 50, 50));
                        }
                    }


                    spriteBatch.Draw(player.texture, destinationRectangle: new Rectangle((int)player.position.X, (int)player.position.Y, 25, 40));

                    //Texture2D rectPer = new Texture2D(graphics.GraphicsDevice, 10, 10);

                    //Color[] data = new Color[10 * 10];
                    //for (int i = 0; i < data.Length; ++i) data[i] = Color.Red;
                    //rectPer.SetData(data);

                    //Vector2 coor = new Vector2(player.position.X + 15, player.position.Y + 12);
                    //spriteBatch.Draw(rectPer, coor, Color.White);


                    spriteBatch.DrawString(font, "Time : " + (Day.currentTime < 10 ? Day.currentTime.ToString().Substring(0, 1) : Day.currentTime.ToString().Substring(0, 2)), new Vector2(0, 0), Color.Black);
                    spriteBatch.DrawString(font, "Day " + Day.count, new Vector2(85, 0), Color.Black);
                }
                else
                {
                    if (Day.count > 7)
                    {
                        if (!won)
                        {
                            soundEffects[1].Play(volume: 0.4f, pitch: 0.0f, pan: 0.0f);
                            won = true;
                        }

                        spriteBatch.Draw(this.Content.Load<Texture2D>("win"), destinationRectangle: new Rectangle(0, 0, 500, 600));
                        //spriteBatch.DrawString(font, "YEAH !", new Vector2(150, 250), Color.Black);
                        //spriteBatch.DrawString(font, "You're not running out of place !", new Vector2(75, 350), Color.Black);
                        //spriteBatch.DrawString(font, "You're enough place for cute flowers.", new Vector2(75, 450), Color.Black);
                    }
                    else
                    {
                        if (!lose)
                        {
                            soundEffects[2].Play(volume: 0.4f, pitch: 0.0f, pan: 0.0f);
                            lose = true;
                        }
                        spriteBatch.Draw(this.Content.Load<Texture2D>("lose"), destinationRectangle: new Rectangle(0, 0, 500, 600));
                        //spriteBatch.DrawString(font, "GAME OVER !", new Vector2(150, 250), Color.Black);
                        //spriteBatch.DrawString(font, "Weeds are conquired your cute garden ! ", new Vector2(75, 350), Color.Black);
                        //gameTime.TotalGameTime.Subtract(gameTime.TotalGameTime);
                    }

                    this.IsMouseVisible = true;

                    Texture2D rect = new Texture2D(graphics.GraphicsDevice, 100, 30);

                    Color[] data = new Color[100 * 30];
                    for (int i = 0; i < data.Length; ++i) data[i] = Color.Gray;
                    rect.SetData(data);

                    Vector2 coor = new Vector2(2, 2);
                    spriteBatch.Draw(rect, coor, Color.White);

                    spriteBatch.DrawString(font, "Try again", new Vector2(7, 4), Color.Black);
                }
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public static LD42 getInstance()
        {
            return LD42Instance;
        }
    }
}
