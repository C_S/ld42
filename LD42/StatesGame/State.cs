﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LD42.Buttons;

namespace LD42.StatesGame
{
    class State
    {
        public String nameState;
        public Rectangle background;
        public Texture2D texture;
        private List<Button> buttons;

        public State (String nameState, Rectangle background, Texture2D texture)
        {
            this.nameState = nameState;
            this.background = background;
            this.texture = texture;
        }

        public State(String nameState, GraphicsDevice gd)
        {
            this.nameState = nameState;

            background = new Rectangle(new Point(0, 0), LD42.getInstance().getSizeWindow());
            switch (nameState)
            {
                case "MENU":
                    this.texture = new Texture2D(gd, background.Width, background.Height);
                    break;
                case "PAUSE":
                    break;
            }
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(LD42.getInstance().Content.Load<Texture2D>("Menu Screens/menuStart"), destinationRectangle: new Rectangle(0, 0, 500, 600));
        }

    }
}
